Format tugas


1.Membuat Database

create database myshop;
 

2. Membuat Table di Dalam Database

Table Users :

 create table users(
    -> id int(8)primary key auto_increment,
    -> nama varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

Table Categories :

create table categories (
    -> id int(8)primary key auto_increment,
    -> nama varchar(255)
    -> );

table items
create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(8),
    -> stock int(8),
    -> category_id int(8),
    -> foreign key(category_id) references categories(id)
    -> );


3.Memasukkan Data pada Table

insert into users(nama) values("John Doe"),("Jane Doe");
insert into users(email) values("john@doe.com"),("jane@doe.com");
insert into users(password) values("john123"),("jenita123");

insert into categories(nama) values("gadget"),("cloth"),("men"),("women"),("branded");

insert into items(name) values("sumsang b50"),("unikloo"),("IMHO watch");
insert into items(description) values("hp keren merks sumsang"),("baju keren ternama"),("jam tangan anak jujur banget");
insert into items(price) values("40000000"),("500000"),("2000000");
insert into items(stock) values("1000"),("50"),("10");
insert into items(category_id) values("1"),("2"),("1");


4.Mengambil Data dari Database

a. Mengambil data users
select id,nama,email from users;

b. Mengambil data items
select * from items where price>1000000;
select * from items where name like '%sang';

c. Menampilkan data items join dengan kategori

select items.name, items.description, items.price, items.stock, items.category_id, categories.nama from items inner join categories on items.category_id = categories.nama;



5.Mengubah Data dari Database

.....